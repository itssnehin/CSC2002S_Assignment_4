JAVAC=/usr/bin/javac
.SUFFIXES: .java .class

SRCDIR=src
BINDIR=bin

$(BINDIR)/%.class:$(SRCDIR)/%.java
	$(JAVAC) -d $(BINDIR)/ -cp $(BINDIR) $<

CLASSES= treeGrow/Land.class treeGrow/Tree.class\
		 treeGrow/SunData.class treeGrow/ForestPanel.class treeGrow/Growth.class\
		treeGrow/Simulation.class treeGrow/TreeGrow.class\

CLASS_FILES=$(CLASSES:%.class=$(BINDIR)/%.class)

default: $(CLASS_FILES)

clean:
	rm $(BINDIR)/*.class