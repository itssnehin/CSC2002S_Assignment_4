package treeGrow;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Land{
	
	
	// sun exposure data here
        int dimX, dimY;
        float[][] sungrid;
        float[][] initGrid;
	static float shadefraction = 0.1f; // only this fraction of light is transmitted by a tree
	
	Land(int dx, int dy) {
		// to do
                dimX = dx;
                dimY = dy;
                initGrid = new float[dimX][dimY];
                sungrid = initGrid;
	}

	// return the number of landscape cells in the x dimension
	int getDimX() {
		return dimX;
	}
	
	// return the number of landscape cells in the y dimension
	int getDimY() {
		return dimY;
	}
	
	// Reset the shaded landscape to the same as the initial sun exposed landscape
	// Needs to be done after each growth pass of the simulator
	void resetShade() {


		sungrid = initGrid;
	}
	
	// return the sun exposure of the initial unshaded landscape at position <x,y?
	float getFull(int x, int y) {
		return sungrid[x][y];
	}
	
	// set the sun exposure of the initial unshaded landscape at position <x,y> to <val>
	void setFull(int x, int y, float val) {
            sungrid[x][y] = val;
	}
	
	// return the current sun exposure of the shaded landscape at position <x,y>
	float getShade(int x, int y) {
		return sungrid[x][y];
	}
	
	// set the sun exposure of the shaded landscape at position <x,y> to <val>
	void setShade(int x, int y, float val){

		sungrid[x][y] = val;
	}
	
	// reduce the sun exposure of the shaded landscape to 10% of the original
	// within the extent of <tree>
	void shadow(int x, int y, float ext){
        
        
            if(ext > 1){
                
                for (int i = x - (int)(ext - 1); i < x + (int)(ext +1); i++) {
                
                    for (int j = y - (int)(ext - 1); j < y + (int)(ext + 1); j++) {
                        try {
                            sungrid[i][j] = 0.1f * sungrid[i][j];
                        } catch (ArrayIndexOutOfBoundsException e) {
                            continue;
                        }
                        
                    }
                }
            }else{

                for (int i = x - (int)ext; i < x + (int)ext; i++) {
                    
                    for (int j = y - (int)ext; i < y + (int)ext; i++) {
                        
                        try {
                            sungrid[i][j] = 0.1f * sungrid[i][j];
                        } catch (Exception e) {
                            continue;
                        }
                    }
                }

            }
	}
}