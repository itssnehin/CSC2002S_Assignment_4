package treeGrow;
import java.util.concurrent.*;
/**
 * Class that performs the simulation by going
 * through different layers and updating variables
 */
public class Simulation{

    Tree[] trees;
    Land land;
    public Simulation(Tree[] trees, Land land){
        this.trees = trees;
        this.land = land;
    }
    
    public void simulate (Tree[] trees, Land land) {
        

        float min =20f; float max = 50f;
        int lo = 0; int hi = trees.length;

        for (int i = 0; i <= 10 ; i++ ) {
            
            generate(land, lo, hi, trees, min, max);
            max = min;
            min -= 2f;

        }

    }


    public void reset (Tree[] trees) {
        land.resetShade();
        for (Tree tree : trees) {
             tree.setExt(0.4f);
         } 
    }

    public void generate(Land landG, int loG, int hiG, Tree[] treesG, float minG, float maxG) {

        ForkJoinPool.commonPool().invoke(new Growth(landG, loG, hiG, treesG, minG, maxG));
    }


      
}