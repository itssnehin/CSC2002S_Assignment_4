package treeGrow;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.*;

/**
 * Calculates the growth for Tree[] using the fork join framework
 */
public class Growth extends RecursiveAction {


    int lo; int hi;
    Land land;
    Tree[] trees;
    int threads = Runtime.getRuntime().availableProcessors() + 1;
    int SEQUENTIAL_CUTOFF = 10000;
    long timeTaken;
    float min, max;

    /**
     * 
     *Constructor that initialises the class
     */
    public Growth(Land land, int lo, int hi, Tree[] trees, float min, float max) {

        this.lo = lo; this.hi = hi;
        this.trees = trees;
        this.land = land;
        this.min = min;
        this.max = max;
    }

    /**
     * calculate and grow the trees concurrently
     * adjust the light values of the land
     */
    protected void compute() {

        if(hi - lo < SEQUENTIAL_CUTOFF) {
            

            for (int i = lo; i < hi; i++) {

                    if (trees[i].inrange(min, max)) {
                        synchronized(this) {trees[i].sungrow(land);}
                        synchronized(this) {land.shadow(trees[i].getX(), trees[i].getY(), trees[i].getExt());}
                    }               
            }


        } else {
            Growth left = new Growth(land, lo, (hi+lo)/2, trees, min, max);
            Growth right = new Growth(land, (hi+lo)/2, hi, trees, min, max);
            left.fork();
            right.compute();
            left.join();
        }
    }

    


}