package treeGrow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.IOException;
import javax.swing.JPanel;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * In order to compile at src:
 * $ javac treeGrow/*.java
 * $ java treeGrow.TreeGrow sample_input.txt
 *
 *
 * 
 */
public class TreeGrow extends JPanel  {

	static Thread t1;
	static volatile SunData sundata;
	static long startTime = 0;
	static int frameX;
	static int frameY;
	static ForestPanel fp;
	static volatile boolean paused;
	static volatile int year = 0;
	static volatile JTextField yearCounterField = new JTextField("Year:  0");
	static volatile Tree[] treeArray;
	static Thread fpt;
	static volatile String msg = "Year: ";
	final static Lock lock = new ReentrantLock();

	// start timer
	private static void tick(){
		startTime = System.currentTimeMillis();
	}
	
	// stop timer, return time elapsed in seconds
	private static float tock(){
		return (System.currentTimeMillis() - startTime) / 1000.0f; 
	}
	/**
	 * initialise the GUI and display the render and the buttons
	 * @param frameX [description]
	 * @param frameY [description]
	 * @param trees  [description]
	 */
	public static void setupGUI(int frameX,int frameY,Tree [] trees) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		Dimension fsize = new Dimension(800, 800); 
		// Frame init and dimensions
    	JFrame frame = new JFrame("Photosynthesis");
    	frame.setLayout(new BorderLayout());
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setPreferredSize(fsize);
    	frame.setSize(800, 800);
    	
      	JPanel g = new JPanel();
        g.setLayout(new BoxLayout(g, BoxLayout.PAGE_AXIS)); 
      	g.setPreferredSize(fsize);
 
		fp = new ForestPanel(trees);
		fp.setPreferredSize(new Dimension(frameX,frameY));
		JScrollPane scrollFrame = new JScrollPane(fp);
		fp.setAutoscrolls(true);
		scrollFrame.setPreferredSize(fsize);
	    g.add(scrollFrame);
        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new GridLayout(1, 6));

        	// reset button
        	JButton btnReset = new JButton ("Reset");
        	btnReset.setActionCommand("Reset");
        	btnReset.addActionListener(new ActionListener() {
        		public void actionPerformed(ActionEvent ae) {

        			System.out.println("Resetting all trees");
        			synchronized(lock) {
        				sundata.sunmap.resetShade();
        				year = 0;
        				for (Tree tree : trees) {
            				tree.setExt(0.4f);
        				}
        			}
        		}
        	});
        	btnPanel.add(btnReset);

        	// Pause button
        	JButton btnPause = new JButton ("Pause");
        	btnPause.setActionCommand("Pause");
        	btnPause.addActionListener(new ActionListener() {
        		public void actionPerformed(ActionEvent ae) {
        			
        			synchronized(lock) {
        				if (!paused) {
        					paused = true;
        					        					
        				}
        			}
        		}
        	});
        	btnPanel.add(btnPause);


        	// Play button - just resumes the simulation
        	JButton btnPlay = new JButton ("Play");
        	btnPlay.setActionCommand("Play");
        	btnPlay.addActionListener(new ActionListener() {
        		public void actionPerformed(ActionEvent ae) {

        			synchronized (t1) {
        				if (paused) {
        					paused = false;
        					t1.interrupt();
        				}
        			}
        			
        			
        		}
        	});
        	btnPanel.add(btnPlay);


        	// End button - Terminates the program.
        	JButton btnEnd = new JButton ("End");
        	btnEnd.setActionCommand("Play");
        	btnEnd.addActionListener(new ActionListener() {
        		public synchronized void actionPerformed(ActionEvent ae) {

        				paused = true;
        				System.exit(0);
        		}
        	});
        	btnPanel.add(btnEnd);

        	
        	btnPanel.add(yearCounterField);

        
    	
      	frame.setLocationRelativeTo(null);  // Center window on screen.
      	frame.add(g, BorderLayout.CENTER); //add contents to window
        //frame.add(bp);
        frame.add(btnPanel, BorderLayout.SOUTH);
        //frame.setContentPane(g);     
        frame.setVisible(true);
        fpt = new Thread(fp);
        fpt.start(); //GUI runs on seperate threads
	}
	
	/**
	 * [main description]
	 * @param args [description]
	 */
	public static void main(String[] args) {
		sundata = new SunData();
		// check that number of command line arguments is correct
		if(args.length != 1)
		{
			System.out.println("Incorrect number of command line arguments. Should have form: java treeGrow.java intputfilename");
			System.exit(0);
		}
				
		// read in forest and landscape information from file supplied as argument
		sundata.readData(args[0]);
		System.out.println("Data loaded");
		treeArray = sundata.trees;

		frameX = sundata.sunmap.getDimX();
		frameY = sundata.sunmap.getDimY();
		setupGUI(frameX, frameY, treeArray);
		
		Simulation sim = new Simulation(treeArray, sundata.sunmap);
		sim.reset(treeArray);

		t1 = new Thread(new TreeGrow().new RunnableImpl(sim)); //Start simulation on different thread
		t1.start();
		
	}

	/**
	 * Class for running the simulation on a different thread
	 */
	private class RunnableImpl implements Runnable { 

		Simulation sim;

		RunnableImpl(Simulation sim) {
			this.sim = sim;
		}
  
        public void run() { 
            while(true) {

			if (!paused) {


				
				year++;
				sim.simulate(treeArray, sundata.sunmap); // simulation 
			
				try {
    				Thread.sleep(200);
				} catch(InterruptedException ex) {
    				Thread.currentThread().interrupt();
    			}

				System.out.println("Year: " + year);

				yearCounterField.setText("Year: " + year);

				
			} else if (paused) {
				synchronized(lock) {

					try {
        				lock.wait();
    	    		} catch (InterruptedException e) {
        					//
      				}
				}
			}

			}
    	} 
	}
} 